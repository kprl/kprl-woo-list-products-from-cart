<?php
/**
 * Plugin Name: kprl Woo List products from cart
 * Plugin URI: https://bitbucket.org/kprl/kprl-wlpfc
 * Bitbucket Plugin URI: https://bitbucket.org/kprl/kprl-woo-list-products-from-cart
 * Description: Functionality to use shortcode to list the producs in WooCommerce shopping cart.
 * Version: 1.0.2
 * Author: Karl Lettenström
 * Author URI: http://kprl.se
 * Text Domain: kprl-wlpfc
 * Domain Path: Optional. Plugin's relative directory path to .mo files. Example: /locale/
 * Network: Optional. Whether the plugin can only be activated network wide. Example: true
 * License: GPL2
 */

/*  Copyright 2018 Karl Lettenström (email : karl@kprl.se)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

function list_products_from_cart() {
  $wlpfcArr = Array();

  // Cycle through each product in the cart
  foreach( WC()->cart->cart_contents as $prod_in_cart ) {
      $prod_id = ( isset( $prod_in_cart['variation_id'] ) && $prod_in_cart['variation_id'] != 0 ) ? $prod_in_cart['variation_id'] : $prod_in_cart['product_id'];

      $prodData = json_decode($prod_in_cart['data']);
      $wlpfcArr[] = "<div class='kprl-woo-list-product'>" . $prod_in_cart['quantity'] . " st. " . $prodData->name . " (ID# " . $prod_in_cart['product_id'] . ") á " . $prodData->price . " SEK.</div>";
  }

  return $wlpfcArr;
}

// Add Shortcode
function kprl_wlpfc() {

  $wlpfcArr = list_products_from_cart();

  ob_start();

  echo "<div class='kprl-woo-list-products-from-cart'>";
  foreach ($wlpfcArr as $key => $wlpfc) {
    echo $wlpfc;
  }
  echo "</div>";

  $content = ob_get_contents();
  ob_end_clean();
  return $content;

}
add_shortcode( 'kprl-wlpfc', 'kprl_wlpfc' );
